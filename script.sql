USE [master]
GO
/****** Object:  Database [Online_Shop]    Script Date: 4/18/2023 4:38:01 PM ******/
CREATE DATABASE [Online_Shop]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Online_Shop', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.KTEAM\MSSQL\DATA\Online_Shop.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Online_Shop_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.KTEAM\MSSQL\DATA\Online_Shop_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Online_Shop] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Online_Shop].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Online_Shop] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Online_Shop] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Online_Shop] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Online_Shop] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Online_Shop] SET ARITHABORT OFF 
GO
ALTER DATABASE [Online_Shop] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Online_Shop] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Online_Shop] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Online_Shop] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Online_Shop] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Online_Shop] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Online_Shop] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Online_Shop] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Online_Shop] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Online_Shop] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Online_Shop] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Online_Shop] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Online_Shop] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Online_Shop] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Online_Shop] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Online_Shop] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Online_Shop] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Online_Shop] SET RECOVERY FULL 
GO
ALTER DATABASE [Online_Shop] SET  MULTI_USER 
GO
ALTER DATABASE [Online_Shop] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Online_Shop] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Online_Shop] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Online_Shop] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Online_Shop] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Online_Shop] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Online_Shop', N'ON'
GO
ALTER DATABASE [Online_Shop] SET QUERY_STORE = OFF
GO
USE [Online_Shop]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 4/18/2023 4:38:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[a_id] [int] IDENTITY(1,1) NOT NULL,
	[a_username] [nvarchar](50) NULL,
	[a_password] [nvarchar](50) NULL,
	[a_manager] [int] NULL,
	[a_admin] [int] NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[a_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bill]    Script Date: 4/18/2023 4:38:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[b_id] [nvarchar](50) NOT NULL,
	[b_dateCreate] [datetime] NULL,
	[b_cname] [nvarchar](50) NULL,
	[b_phone] [nvarchar](50) NULL,
	[b_address] [nvarchar](50) NULL,
	[b_total] [nvarchar](50) NULL,
	[b_status] [int] NULL,
	[cus_id] [int] NULL,
 CONSTRAINT [PK_Bill] PRIMARY KEY CLUSTERED 
(
	[b_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bill_Detail]    Script Date: 4/18/2023 4:38:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill_Detail](
	[bd_id] [int] NOT NULL,
	[p_id] [nvarchar](50) NOT NULL,
	[b_id] [nvarchar](50) NOT NULL,
	[bd_quantity] [int] NULL,
	[bd_price] [money] NULL,
	[bd_total] [money] NULL,
 CONSTRAINT [PK_Bill_Detail] PRIMARY KEY CLUSTERED 
(
	[bd_id] ASC,
	[p_id] ASC,
	[b_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 4/18/2023 4:38:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[c_id] [int] NOT NULL,
	[c_name] [nvarchar](50) NULL,
	[c_describe] [nvarchar](150) NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[c_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Color]    Script Date: 4/18/2023 4:38:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Color](
	[co_id] [int] NOT NULL,
	[color] [nvarchar](50) NULL,
 CONSTRAINT [PK_Color] PRIMARY KEY CLUSTERED 
(
	[co_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 4/18/2023 4:38:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[cus_id] [int] NOT NULL,
	[cus_name] [nvarchar](50) NOT NULL,
	[cus_phone] [nvarchar](50) NULL,
	[cus_address] [nvarchar](100) NULL,
	[cus_username] [nvarchar](50) NOT NULL,
	[cus_password] [nvarchar](50) NOT NULL,
	[cus_status] [bit] NULL,
	[cus_type] [bit] NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[cus_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 4/18/2023 4:38:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[p_id] [nvarchar](50) NOT NULL,
	[p_name] [nvarchar](150) NULL,
	[p_quantity] [int] NULL,
	[p_price] [money] NULL,
	[p_releaseDate] [date] NULL,
	[p_describe] [nvarchar](150) NULL,
	[p_image] [nvarchar](max) NULL,
	[c_id] [int] NULL,
	[a_manager] [int] NULL,
	[s_id] [int] NULL,
	[co_id] [int] NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[p_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Size]    Script Date: 4/18/2023 4:38:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Size](
	[s_id] [int] NOT NULL,
	[size] [nvarchar](50) NULL,
 CONSTRAINT [PK_Size] PRIMARY KEY CLUSTERED 
(
	[s_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON 

INSERT [dbo].[Account] ([a_id], [a_username], [a_password], [a_manager], [a_admin]) VALUES (1, N'admin', N'123', 0, 1)
INSERT [dbo].[Account] ([a_id], [a_username], [a_password], [a_manager], [a_admin]) VALUES (2, N'seller', N'123', 1, 0)
INSERT [dbo].[Account] ([a_id], [a_username], [a_password], [a_manager], [a_admin]) VALUES (3, N'mra', N'123', 0, 0)
INSERT [dbo].[Account] ([a_id], [a_username], [a_password], [a_manager], [a_admin]) VALUES (4, N'mrb', N'123', 0, 0)
INSERT [dbo].[Account] ([a_id], [a_username], [a_password], [a_manager], [a_admin]) VALUES (5, N'mra', N'123', 0, 0)
INSERT [dbo].[Account] ([a_id], [a_username], [a_password], [a_manager], [a_admin]) VALUES (6, N'mrd', N'123', 0, 0)
INSERT [dbo].[Account] ([a_id], [a_username], [a_password], [a_manager], [a_admin]) VALUES (1005, N'mrs', N'123', 0, 0)
SET IDENTITY_INSERT [dbo].[Account] OFF
GO
INSERT [dbo].[Categories] ([c_id], [c_name], [c_describe]) VALUES (1, N'Áo sơ mi ngắn tay', N'New')
INSERT [dbo].[Categories] ([c_id], [c_name], [c_describe]) VALUES (2, N'Áo sợ mi dài tay', N'New')
INSERT [dbo].[Categories] ([c_id], [c_name], [c_describe]) VALUES (3, N'Áo khoác gió', N'New')
INSERT [dbo].[Categories] ([c_id], [c_name], [c_describe]) VALUES (4, N'Áo khoác', N'New')
INSERT [dbo].[Categories] ([c_id], [c_name], [c_describe]) VALUES (5, N'Bộ Nỉ', N'New')
INSERT [dbo].[Categories] ([c_id], [c_name], [c_describe]) VALUES (6, N'Áo Polo dài tay', N'New')
INSERT [dbo].[Categories] ([c_id], [c_name], [c_describe]) VALUES (7, N'Áo len', N'New')
INSERT [dbo].[Categories] ([c_id], [c_name], [c_describe]) VALUES (8, N'Áo thun giữ nhiệt', N'New')
GO
INSERT [dbo].[Color] ([co_id], [color]) VALUES (1, N'Blacks')
INSERT [dbo].[Color] ([co_id], [color]) VALUES (2, N'Whites')
INSERT [dbo].[Color] ([co_id], [color]) VALUES (3, N'Reds')
INSERT [dbo].[Color] ([co_id], [color]) VALUES (4, N'Greys')
INSERT [dbo].[Color] ([co_id], [color]) VALUES (5, N'Blues')
INSERT [dbo].[Color] ([co_id], [color]) VALUES (6, N'Beige Tones')
INSERT [dbo].[Color] ([co_id], [color]) VALUES (7, N'Greens')
INSERT [dbo].[Color] ([co_id], [color]) VALUES (8, N'Yellows')
GO
INSERT [dbo].[Products] ([p_id], [p_name], [p_quantity], [p_price], [p_releaseDate], [p_describe], [p_image], [c_id], [a_manager], [s_id], [co_id]) VALUES (N'1', N'Áo khoác Jean Nam Nữ cao cấp', 5, 588.0000, CAST(N'2023-02-02' AS Date), N'Good', N'https://st.app1h.com/uploads/images/company72/images/ao-thun-nam-chat-1.jpg', 1, 1, 1, 1)
INSERT [dbo].[Products] ([p_id], [p_name], [p_quantity], [p_price], [p_releaseDate], [p_describe], [p_image], [c_id], [a_manager], [s_id], [co_id]) VALUES (N'2', N'ÁO KHOÁC NAM HADTR HÀNG MÙA XUÂN', 2, 950.0000, CAST(N'2023-02-22' AS Date), N'Good', N'https://media.gq-magazine.co.uk/photos/6176b776cbf71cd06266d570/master/w_1600%2Cc_limit/251021_MS_20.jpg', 1, 1, 1, 1)
INSERT [dbo].[Products] ([p_id], [p_name], [p_quantity], [p_price], [p_releaseDate], [p_describe], [p_image], [c_id], [a_manager], [s_id], [co_id]) VALUES (N'23', N'12sdf', 5, 544.0000, NULL, N'Bad', N'https://pyxis.nymag.com/v1/imgs/fac/416/15cff983709af0f8876b3d4382ccaedccd-framelehighskinnyjeans2.png', 1, 1, NULL, NULL)
INSERT [dbo].[Products] ([p_id], [p_name], [p_quantity], [p_price], [p_releaseDate], [p_describe], [p_image], [c_id], [a_manager], [s_id], [co_id]) VALUES (N'3', N'Áo rét', 3, 12.0000, CAST(N'2023-02-22' AS Date), N'Good', N'https://cf.shopee.vn/file/50355d62bcf01fbc08749c98506053c1', 2, 1, 1, 2)
INSERT [dbo].[Products] ([p_id], [p_name], [p_quantity], [p_price], [p_releaseDate], [p_describe], [p_image], [c_id], [a_manager], [s_id], [co_id]) VALUES (N'4', N'Váy Thanh Lịch', 2, 450.0000, CAST(N'2023-02-22' AS Date), N'Good', N'https://i.dailymail.co.uk/i/pix/2016/02/02/16/3057466900000578-3427967-image-a-238_1454429284791.jpg', 3, 1, 2, 2)
INSERT [dbo].[Products] ([p_id], [p_name], [p_quantity], [p_price], [p_releaseDate], [p_describe], [p_image], [c_id], [a_manager], [s_id], [co_id]) VALUES (N'56', N'Áo rét', 43, 234.0000, CAST(N'2023-02-22' AS Date), N'Good', N'https://i.pinimg.com/736x/34/7b/32/347b3214800619a2ef54eb944dd1966b--men-fashion-casual-fashion-man.jpg', 3, 1, 2, 2)
GO
INSERT [dbo].[Size] ([s_id], [size]) VALUES (1, N'XXS')
INSERT [dbo].[Size] ([s_id], [size]) VALUES (2, N'XS')
INSERT [dbo].[Size] ([s_id], [size]) VALUES (3, N'XS-S')
INSERT [dbo].[Size] ([s_id], [size]) VALUES (4, N'S')
INSERT [dbo].[Size] ([s_id], [size]) VALUES (5, N'M')
INSERT [dbo].[Size] ([s_id], [size]) VALUES (6, N'M-L')
INSERT [dbo].[Size] ([s_id], [size]) VALUES (7, N'L')
INSERT [dbo].[Size] ([s_id], [size]) VALUES (8, N'XL')
GO
ALTER TABLE [dbo].[Bill]  WITH CHECK ADD  CONSTRAINT [FK_Bill_Customers] FOREIGN KEY([cus_id])
REFERENCES [dbo].[Customers] ([cus_id])
GO
ALTER TABLE [dbo].[Bill] CHECK CONSTRAINT [FK_Bill_Customers]
GO
ALTER TABLE [dbo].[Bill_Detail]  WITH CHECK ADD  CONSTRAINT [FK_Bill_Detail_Bill] FOREIGN KEY([b_id])
REFERENCES [dbo].[Bill] ([b_id])
GO
ALTER TABLE [dbo].[Bill_Detail] CHECK CONSTRAINT [FK_Bill_Detail_Bill]
GO
ALTER TABLE [dbo].[Bill_Detail]  WITH CHECK ADD  CONSTRAINT [FK_Bill_Detail_Products] FOREIGN KEY([p_id])
REFERENCES [dbo].[Products] ([p_id])
GO
ALTER TABLE [dbo].[Bill_Detail] CHECK CONSTRAINT [FK_Bill_Detail_Products]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Account] FOREIGN KEY([a_manager])
REFERENCES [dbo].[Account] ([a_id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Account]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Categories] FOREIGN KEY([c_id])
REFERENCES [dbo].[Categories] ([c_id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Categories]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Color] FOREIGN KEY([co_id])
REFERENCES [dbo].[Color] ([co_id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Color]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Size] FOREIGN KEY([s_id])
REFERENCES [dbo].[Size] ([s_id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Size]
GO
USE [master]
GO
ALTER DATABASE [Online_Shop] SET  READ_WRITE 
GO
