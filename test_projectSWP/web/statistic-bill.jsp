<%-- 
    Document   : statistic-bill
    Created on : Apr 25, 2023, 7:24:27 PM
    Author     : vanhv
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SWP391.G1 Statistic</title>  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>

    <body class="container" style="width:90%">
        <div class="table-wrapper">
            <div class="table-title">

                <div class="row">
                    <div class="col-sm-6">
                        <h3>Bill Management</h3>
                        <div style="display: flex">
                            <form method="/manage-bill?service=displayAll" style="margin-bottom: 20px; margin-right: 5px">
                                <input type="date" name="date" value="${date}"/>
                                <button type="submit" style="background-color:#e6ccff;border-color: transparent">Find</button>
                            </form>
                            <form method="/manage-bill?service=displayAll" style="margin-bottom: 20px">
                                <button type="submit" style="background-color:#c2c2d6;border-color: transparent">Reset</button>
                            </form>
                        </div>
                    </div>
                    <h3 class="text-danger" style="color: white"> ${mess}</h3>

                </div>
                <table class="table table-striped table-hover ">
                    <tr>
                        <th>Code</th>
                        <th>Order Date</th>
                        <th>Receiver</th>
                        <th>Phone</th>
                        <th>Delivery Address</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <c:set var="grandTotal" value="${0}"/>
                    <c:forEach items="${listBill}" var="list">
                        <tr>
                            <td>${list.oID}</td>
                            <td>${list.dateCreate}</td>
                            <td>${list.cname}</td>
                            <td>${list.cphone}</td>
                            <td>${list.cAddress}</td>
                            <td>${list.total}$</td>
                            <td style="color:${list.status==0?"#ff9900"
                                               :(list.status==1?"#0099ff"
                                               :(list.status==-1?"#ff0000"
                                               :"#00cc00"))};
                                font-weight:600 ">
                                ${list.status==0?"WAITING"
                                  :(list.status==1?"DELIVERING"
                                  :(list.status==-1?"CANCELED"
                                  :"RECEIVED"))}
                            </td>
                            <td>
                                <a href="/manage-bill?service=detail&b_id=${list.oID}">
                                    <button type="button" class="btn-info">Detail</button>
                                </a>
                            </td>
                            <td>
                                <c:if test="${list.status==0}">
                                    <a href="/manage-bill?service=deliver&b_id=${list.oID}">
                                        <button type="button" class="btn-success">Deliver</button>
                                    </a>
                                </c:if>
                                <c:if test="${list.status==1}">
                                    <a href="/manage-bill?service=cancelDelivery&b_id=${list.oID}">
                                        <button type="button" class="btn-warning">Cancel delivery</button>
                                    </a>
                                </c:if>
                            </td>
                            <td>
                                <c:if test="${list.status!=-1}">
                                    <a href="/manage-bill?service=cancelOrder&b_id=${list.oID}">
                                        <button type="button" class="btn-danger">Cancel</button>
                                    </a> 
                                </c:if>
                            </td>
                        </tr>
                        <c:set var="grandTotal" value="${grandTotal+list.total}"/>
                    </c:forEach>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Grand total: ${grandTotal}$</th>
                    </tr>
                </table>

            </div>
            <a href="shopp"><button type="button" class="btn btn-primary">Back to Shop</button></a>
        </div>
    </body>
</html>
