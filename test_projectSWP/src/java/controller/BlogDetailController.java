/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.BlogDAO;
//import dal.CommentDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import model.Blog;
//import model.Comment;

/**
 *
 * @author tomlo
 */
@WebServlet(name = "BlogDetailController", urlPatterns = {"/blogdetail"})
public class BlogDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    BlogDAO blogdao = new BlogDAO();
//    CommentDAO commentdao = new CommentDAO();
    Blog b;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("Do get");
        String id = request.getParameter("id");
        b = blogdao.getBlogByID(id);
//        List<Comment> list = commentdao.getAllComment(b.getId() + "");

        request.setAttribute("detail", b);
//        request.setAttribute("list", list);
        request.getRequestDispatcher("blogdetail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("Do post");
        String content = request.getParameter("comment");
        int id = b.getId();
        int id_account = 1;
        Date date = new Date();
        Timestamp createDate = new Timestamp(date.getTime());
//        int check = commentdao.insertNewComment(new Comment(content, id, id_account, createDate));
//        if (check > 0) {
//            System.out.println("Insert Success");
//        } else {
//            System.out.println("Insert Fails");
//        }
        request.getRequestDispatcher("blogdetail").forward(request, response);
        return;
    }

    /**
     * Returns a short description of the .
     *
     * @return a String containing  description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
