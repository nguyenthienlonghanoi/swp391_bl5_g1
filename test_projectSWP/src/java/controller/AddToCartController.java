package controller;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
import dal.DAOBill;
import dal.DAOCustomer;
import dal.DAOProduct;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import model.Bill;
import model.BillDetail;
import model.Cart;
import model.Customer;
import model.Products;
import untils.Helpers;

/**
 *
 * @author vanhv
 */
@WebServlet(urlPatterns = {"/cartcontroller"})
public class AddToCartController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DAOProduct daoP = new DAOProduct();
        DAOCustomer daoC = new DAOCustomer();
        DAOBill daoB = new DAOBill();
        Helpers helpers = new Helpers();
        HttpSession session = request.getSession();

        try ( PrintWriter out = response.getWriter()) {
            String service = request.getParameter("service");
            if (service == null) {
                service = "showcart";
            }
            if (service.equals("showcart")) {
                Enumeration em = session.getAttributeNames();

                DateFormat format = new SimpleDateFormat();
                Date currentDate = new Date();
                String getDate = format.format(currentDate);

                ArrayList<Cart> listCart = new ArrayList<>();
                while (em.hasMoreElements()) {
                    String pid = em.nextElement().toString();
                    if (pid.equalsIgnoreCase("listCart")) {
                        continue;
                    }
                    String quantity = session.getAttribute(pid).toString();
                    Cart cart = daoP.getProductPrice(pid, getDate, quantity);
                    if (cart != null) {
                        listCart.add(cart);
                    }
                }
                session.setAttribute("listCart", listCart);
                request.getRequestDispatcher("/showcart.jsp").forward(request, response);
            }
            if (service.equals("addtocart")) {
                String quan = request.getParameter("quantity");
                String pid = request.getParameter("p_id");

                Object value = session.getAttribute(pid);
                if (value == null) {
                    session.setAttribute(pid, quan);
                    request.getRequestDispatcher("shopp?service=displayAll").forward(request, response);
                } else {
                    int preQuantity = Integer.parseInt(quan);
                    int oldQuantity = Integer.parseInt(value.toString());
                    Products p = daoP.getMaxQuantityByID(pid);
                    int newquantity = oldQuantity + preQuantity;
                    if (newquantity <= p.getQuantity()) {
                        session.setAttribute(pid, newquantity);
                    } else if (oldQuantity + 1 < p.getQuantity()) {
                        session.setAttribute(pid, oldQuantity + 1);
                    } else {
                        session.setAttribute(pid, oldQuantity + 0);
                    }
                    request.getRequestDispatcher("shopp?service=displayAll").forward(request, response);
                }
            }
            if (service.equals("update")) {
                Enumeration em = session.getAttributeNames();
                while (em.hasMoreElements()) {
                    String pid = em.nextElement().toString();
                    if (pid.equalsIgnoreCase("listCart")) {
                        continue;
                    }
                    String getQuantityIdbyName = "p" + pid;
                    String quantity = request.getParameter(getQuantityIdbyName);
                    Products prod = daoP.getMaxQuantityByID(pid);
                    if (prod != null) {
                        if (Integer.parseInt(quantity) <= prod.getQuantity()) {
                            session.setAttribute(pid, quantity);
                        }
                    }
                }
                request.getRequestDispatcher("/cartcontroller?service=showcart").forward(request, response);
            }
            if (service.equals("checkout")) {
                request.getRequestDispatcher("/checkout.jsp").forward(request, response);
            }
            if (service.equals("remove")) {
                String id = request.getParameter("pid");
                session.removeAttribute(id);
                request.getRequestDispatcher("/cartcontroller?service=showcart").forward(request, response);
            }
            if (service.equals("removeAll")) {
                Enumeration em = session.getAttributeNames();
                while (em.hasMoreElements()) {
                    String pid = em.nextElement().toString();
                    if (pid.equalsIgnoreCase("listCart") || pid.equalsIgnoreCase("username")
                            || pid.equalsIgnoreCase("acc")) {
                        continue;
                    }
                    session.removeAttribute(pid);
                }
                request.getRequestDispatcher("/cartcontroller?service=showcart").forward(request, response);
            }
            if (service.equals("payment")) {
                String randomOderId = helpers.randomOid();
                ArrayList<Cart> listCart = (ArrayList<Cart>) session.getAttribute("listCart");
                //Check cart isEmpty? insert : buy

                String receiverName = request.getParameter("name");
                String receiverEmail = request.getParameter("email");
                String receiverPhone = request.getParameter("phone");
                String receiverAddress = request.getParameter("address");

                if (listCart != null) {
                    //SET get date
                    DateFormat format = new SimpleDateFormat();
                    Date currentDate = new Date();
                    String getDate = format.format(currentDate);
                    double grandTotal = 0;
                    for (Cart c : listCart) {
                        grandTotal += c.getTotal();
                    }
//                    //New Bill
                    Object username = session.getAttribute("username");
                    if (username == null) {
                        Bill bill = new Bill(randomOderId, getDate, receiverName,
                                receiverPhone, receiverAddress, grandTotal, 0, null);
                        daoB.CreateBill(bill);
                    } else {
                        String usernameS = String.valueOf(username);
                        Customer cus = daoC.getBustomerByUsername(usernameS);
                        Bill bill = new Bill(
                                randomOderId,
                                getDate,
                                receiverName,
                                receiverPhone, receiverAddress, grandTotal, 0,
                                String.valueOf(cus.getCid()));
                        daoB.CreateBill(bill);
                    }
                    for (Cart c : listCart) {
                        String pid = c.getpId();
                        int quantity = Integer.valueOf(c.getcQuantity());
                        double price = c.getPrice();
                        double total = c.getTotal();
                        BillDetail billDetail
                                = new BillDetail(pid, randomOderId, quantity, price, total);
                        daoB.createBillDetail(billDetail);
                    }
                    Enumeration em = session.getAttributeNames();
                    while (em.hasMoreElements()) {
                        String id = em.nextElement().toString();
                        if (id.equalsIgnoreCase("password") || id.equalsIgnoreCase("acc")
                                || id.equalsIgnoreCase("username")) {
                            continue;
                        }
                        session.removeAttribute(id);
                    }
                }
                response.sendRedirect("shopp");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
