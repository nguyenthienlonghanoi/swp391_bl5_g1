/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAOBill;
import dal.DAOCustomer;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Bill;
import model.BillDetail;
import model.Customer;

/**
 *
 * @author vanhv
 */
@WebServlet(name = "PurchaseController", urlPatterns = {"/purchase"})
public class PurchaseController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            String service = request.getParameter("service");
            HttpSession session = request.getSession(true);
            DAOCustomer daoC = new DAOCustomer();
            DAOBill daoB = new DAOBill();
            if (service == null) {
                service = "displayAll";
            }
            if (service.equals("displayAll")) {
                String username = (String) session.getAttribute("username");
                Customer cus = daoC.getCustomerAccountByUsername(username);
                ArrayList<Bill> listBill = daoB.getBillByUserId(cus.getCid());
                request.setAttribute("listBill", listBill);
                request.getRequestDispatcher("/purchase.jsp").forward(request, response);
            }
            if (service.equals("detail")) {
                String b_id = request.getParameter("b_id");
                ArrayList<BillDetail> billDetail = daoB.getBillDetailById(b_id);
                request.setAttribute("listBillDetail", billDetail);
                request.getRequestDispatcher("purchase-detail.jsp").forward(request, response);
            }
            if (service.equals("cancel")) {
                String b_id = request.getParameter("b_id");
                daoB.cancelOrder(b_id);
                request.getRequestDispatcher("purchase?service=displayAll").forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
