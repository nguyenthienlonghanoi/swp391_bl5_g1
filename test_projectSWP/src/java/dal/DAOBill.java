/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Bill;
import model.BillDetail;

/**
 *
 * @author vanhv
 */
public class DAOBill extends DBContext {

    public void cancelOrder(String b_id) {
        updateStatusById(b_id, -1);
    }

    public void updateStatusById(String b_id, int status) {
        String sql = "update Bill set b_status = ? where b_id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, status);
            st.setString(2, b_id);
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    public ArrayList<BillDetail> getBillDetailById(String b_id) {
        ArrayList<BillDetail> list = new ArrayList<>();
        String sql = "select B.bd_quantity,B.bd_price,B.bd_total, P.p_name,P.p_image,P.p_describe\n"
                + "from Bill_Detail B\n"
                + "inner join Products P on B.p_id= P.p_id and B.b_id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, b_id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                BillDetail bill = new BillDetail(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6)
                );
                list.add(bill);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public ArrayList<Bill> getAllBillByDate(String date) {
        ArrayList<Bill> list = new ArrayList<>();
        String sql = "select * from Bill where CONVERT(date,b_dateCreate)=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, date);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Bill bill = new Bill(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getString(8)
                );
                list.add(bill);
            }
        } catch (SQLException e) {
        }
        return list;
    }
    public ArrayList<Bill> getAllBill() {
        ArrayList<Bill> list = new ArrayList<>();
        String sql = "select * from Bill";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Bill bill = new Bill(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getString(8)
                );
                list.add(bill);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public ArrayList<Bill> getBillByUserId(int cid) {
        ArrayList<Bill> list = new ArrayList<>();
        String sql = "select * from Bill where cus_id=? order by b_dateCreate";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, cid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Bill bill = new Bill(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getString(8)
                );
                list.add(bill);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public void CreateBill(Bill obj) {
        String sql = "insert into Bill(b_id,b_dateCreate,b_cname,b_phone,b_address,b_total,b_status,cus_id)"
                + "values(?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, obj.getoID());
            st.setString(2, obj.getDateCreate());
            st.setString(3, obj.getCname());
            st.setString(4, obj.getCphone());
            st.setString(5, obj.getcAddress());
            st.setDouble(6, obj.getTotal());
            st.setInt(7, obj.getStatus());
            if (obj.getCid() == null) {
                st.setString(8, null);
            } else {
                st.setInt(8, Integer.parseInt(obj.getCid()));
            }
            st.executeUpdate();
            System.out.println("CREATE SUCCESS");
        } catch (SQLException e) {
        }
    }

    public void createBillDetail(BillDetail obj) {
        String sql = "insert into Bill_Detail(p_id,b_id,bd_quantity,bd_price,bd_total)"
                + "values(?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, obj.getPid());
            st.setString(2, obj.getBid());
            st.setInt(3, obj.getQuantity());
            st.setDouble(4, obj.getPrice());
            st.setDouble(5, obj.getTotal());
            st.executeUpdate();
            System.out.println("CREATE SUCCESS");
        } catch (SQLException e) {
        }
    }

    public static void main(String[] args) {
//        DAOBill db = new DAOBill();
//        DateFormat format = new SimpleDateFormat();
//        Date currentDate = new Date();
//        db.CreateBill(new Bill("B2", "2023/04/21", "cName", "cPhone", "cAddress", 99, 1, "2"));
    }
}
