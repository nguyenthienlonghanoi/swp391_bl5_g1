/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Blog;

/**
 *
 * @author tomlo
 */
public class BlogDAO extends DBContext {

//get all blog
    public List<Blog> getAllBlog() {
        List<Blog> list = new ArrayList<>();
        String sql = "select * from Blog";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Blog(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getTimestamp(8)));
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public Blog getBlogByID(String id) {
        Blog b = new Blog();
        String sql = "select * from Blog where id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                b = new Blog(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getTimestamp(8));
            }
        } catch (SQLException e) {
        }
        return b;
    }

    public int insertNewBlog(Blog blog) {
        String sql = "INSERT INTO Blog\n"
                + "(image, title, content, author, status, num_reactions, createTime)\n"
                + "VALUES(?, ?, ?, ?, ?, ?, ?);";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, blog.image);
            st.setString(2, blog.title);
            st.setString(3, blog.context);
            st.setInt(4, blog.author);
            st.setInt(5, blog.status);
            st.setInt(6, blog.numReact);
            st.setString(7, blog.createAt.toString());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.toString());
            return 0;
        }
        return 1;
    }

    public static void main(String[] args) {
        BlogDAO dao = new BlogDAO();
        List<Blog> list = dao.getAllBlog();
        for (Blog b : list) {
            System.out.println(b);
        }
        Date date = new Date();
        Timestamp createDate = new Timestamp(date.getTime());
        int check = dao.insertNewBlog(new Blog("blog-1.jpg", "Minh", "Minh", 1, 1, 0,createDate));
        System.out.println(check);
    }
}
