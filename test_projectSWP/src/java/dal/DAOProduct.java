/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cart;
import model.Color;
import model.Products;
import model.Size;

/**
 *
 * @author vanhv
 */
public class DAOProduct extends DBContext {

    public ArrayList<Products> getMaxQuantity() {
        ArrayList<Products> list = new ArrayList<>();
        String query = "select p_id ,p_quantity from Products where status=1";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Products(rs.getString(1), rs.getInt(2)));
            }
        } catch (SQLException e) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, e);
        }
        return list;
    }

    public Cart getProductPrice(String pid, String date, String quantity) {
        Cart cart = null;
        String query = "select p_name, p_price, p_quantity from Products where p_id ='" + pid + "'";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String name = rs.getString(1);
                double price = rs.getDouble(2);
                double total = Double.valueOf(quantity) * price;
                cart = new Cart(pid, date, name, quantity, price, total, String.valueOf(rs.getInt(3)));
            }
        } catch (SQLException e) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, e);
        }
        return cart;
    }

    public Products getMaxQuantityByID(String id) {
        Products prod = null;
        String query = "select p_id ,p_quantity from Products where status=1 and p_id='" + id + "'";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                prod = new Products(rs.getString(1), rs.getInt(2));
            }
        } catch (SQLException e) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, e);
        }
        return prod;
    }

    public ArrayList<Size> getListSize() {
        ArrayList<Size> list = new ArrayList<>();
        String querry = "select * from size";
        try {
            PreparedStatement st = connection.prepareStatement(querry);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Size(rs.getInt(1), rs.getString(2)));
            }
        } catch (SQLException e) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, e);
        }
        return list;
    }

    public ArrayList<Color> getListColor() {
        ArrayList<Color> list = new ArrayList<>();
        String querry = "select * from color";
        try {
            PreparedStatement st = connection.prepareStatement(querry);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Color(rs.getInt(1), rs.getString(2)));
            }
        } catch (SQLException e) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, e);
        }
        return list;
    }

    public static void main(String[] args) {
        DAOProduct daoP = new DAOProduct();
        ArrayList<Products> list = daoP.getMaxQuantity();
        ArrayList<Size> listSize = daoP.getListSize();
        ArrayList<Color> listColor = daoP.getListColor();
//        for (Products p : list) {
//            System.out.println(p.getId() + " - " + p.getQuantity());
//        }
        for (Size s : listSize) {
            System.out.println(s.toString());
        }

    }

}
