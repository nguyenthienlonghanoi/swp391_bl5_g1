/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Customer;

/**
 *
 * @author vanhv
 */
public class DAOCustomer extends DBContext {

    public Customer getCustomerAccountByUsername(String username) {
        Customer cus = null;
        String sql = "select C.cus_id,C.cus_username from Customers C\n"
                + "right join Account A\n"
                + "on C.cus_username='" + username + "'";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                cus = new Customer(rs.getInt(1), rs.getString(2));
            }
        } catch (Exception e) {
        }
        return cus;
    }

    public void insertCustomer(String name, String username, String password) {
        String sql = "insert into Customers(cus_name,cus_phone, cus_address,cus_username,cus_password,cus_status,cus_type)\n"
                + "values(?,'01239876','Ha Noi',?,?,1,1)";
        try {
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setString(1, name);
            pre.setString(2, username);
            pre.setString(3, password);

            pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOBill.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Customer getBustomerByUsername(String username) {
        Customer customer = null;
        String sql = "select * from Customers where cus_username='" + username + "'";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                customer = new Customer(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getInt(8)
                );
            }
        } catch (Exception e) {
        }
        return customer;
    }

    public static void main(String[] args) {
        DAOCustomer dao = new DAOCustomer();
        System.out.println(dao.getBustomerByUsername("cus1"));
    }
}
