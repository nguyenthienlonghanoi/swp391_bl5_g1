/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.Category;
import model.Products;

/**
 *
 * @author dell
 */
public class DAO extends DBContext {

//lay all product
    public List<Products> getAllProduct() {
        List<Products> list = new ArrayList<>();
        String sql = "select * from Products";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Products(rs.getString(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        rs.getInt(11),
                        rs.getInt(12)
                ));
            }
        } catch (SQLException e) {
        }
        return list;
    }
    
    public List<Products> search(String search) {
        List<Products> list = new ArrayList<>();
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement("SELECT * FROM Products WHERE p_name like '%'+?+'%'");
            st.setString(1, search);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Products(rs.getString(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        rs.getInt(11),
                        rs.getInt(12)
                ));
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return list;
    }
    
    public List<Products> filter(String min, String max) {
        List<Products> list = new ArrayList<>();
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement("SELECT * FROM Products WHERE p_price BETWEEN ? AND ?");
            st.setString(1, min);
            st.setString(2, max);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Products(rs.getString(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        rs.getInt(11),
                        rs.getInt(12)
                ));
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return list;
    }
    
    public int getNumberOfProduct() {
        int count = 0;
        String sql = "select count(*) from Products";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
        }
        return count;
    }
    
    public List<Products> pagingProduct(int index) {
        List<Products> list = new ArrayList<>();
        String sql = "SELECT * FROM Products where status=1 ORDER BY p_id OFFSET ? ROWS FETCH NEXT 6 ROW ONLY;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, (index - 1) * 6);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Products(rs.getString(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        rs.getInt(11),
                        rs.getInt(12)
                ));
            }
        } catch (SQLException e) {
        }
        return list;
    }
    
    public List<Products> getAllActiveProduct() {
        List<Products> list = new ArrayList<>();
        String sql = "select * from Products where status=1";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Products(rs.getString(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        rs.getInt(11),
                        rs.getInt(12)
                ));
            }
        } catch (SQLException e) {
        }
        return list;
    }
//lay all category

    public List<Category> getAllCategory() {
        List<Category> list = new ArrayList<>();
        String sql = "select * from Categories";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Category(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3)));

            }
        } catch (SQLException e) {
        }
        return list;
    }
//lay san pham noi bat nhat

    public Products getHotProduct() {
        List<Category> list = new ArrayList<>();
        String sql = "select top 1 * from Products order by p_id desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Products(rs.getString(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        rs.getInt(11),
                        rs.getInt(12));
            }
        } catch (SQLException e) {
        }
        return null;
    }
//lay san pham theo loai category

    public List<Products> getProductByCatagoryID(String cid) {
        List<Products> list = new ArrayList<>();
        String sql = "select * from Products where c_id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, cid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Products(rs.getString(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        rs.getInt(11),
                        rs.getInt(12)));
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public List<Account> getAllAcc() {
        List<Account> list = new ArrayList<>();
        String sql = "select * from Account where a_admin <> 1";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Account acc = new Account(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7)
                );
                list.add(acc);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public void BlockAccount(String aid) {
        String query = "update Account set [a_status] = -1 where [a_id] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, aid);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void UnBlockAccount(String aid) {
        String query = "update Account set [a_status] =0  where [a_id] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, aid);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public int updateNewPassword(String email, String newPassword) {
        String sql = "update Account set a_password = ? where a_email = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, newPassword);
            st.setString(2, email);
            st.executeUpdate();
            return 1;
        } catch (SQLException e) {
            return 0;
        }
    }

    public Account checkEmail(String email) {
        String sql = "select * from Account where a_email = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Account(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7)
                );
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public List<Products> getProductByManagerID(int cid) {
        List<Products> list = new ArrayList<>();
        String sql = "select * from Products where a_manager = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, cid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Products(rs.getString(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        rs.getInt(11),
                        rs.getInt(12)));
            }
        } catch (SQLException e) {
        }
        return list;
    }
//lay san pham bang id

    public Products getProductByID(String id) {

        String sql = "select * from Products where p_id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Products(rs.getString(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        rs.getInt(11),
                        rs.getInt(12));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public Account login(String user, String pass) {
        String sql = "select * from Account where a_username = ? and a_password = ? and a_status<>-1";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            st.setString(2, pass);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Account(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7));

            }
        } catch (SQLException e) {
        }
        return null;
    }

    public Account checkAccountExist(String user) {
        String sql = "select * from Account where a_username = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Account(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public void sigup(String user, String pass,String email) {
        String sql = "insert into Account values(?, ?, 0, 0,?,0)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            st.setString(2, pass);
            st.setString(3, email);
            st.executeUpdate();
        } catch (SQLException e) {

        }
    }

    public void insertProduct(Products obj) {
        String sql = "insert into Products (p_name,p_quantity,p_price,p_describe,p_image,c_id,a_manager,status)"
                + " values(" + obj.addprod() + ")";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.executeUpdate();
        } catch (SQLException e) {

        }
    }

    public void editProduct(String name, int quantity, double price, String describe, String img, int category, String id, int status) {
        String sql = "UPDATE [dbo].[Products]\n"
                + "   SET [p_name] = ?\n"
                + "      ,[p_quantity] = ?\n"
                + "      ,[p_price] = ?\n"
                + "      ,[p_describe] = ?\n"
                + "      ,[p_image] = ?\n"
                + "      ,[c_id] = ?\n"
                + "      ,[status] = ?\n"
                + " WHERE [p_id] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, name);
            st.setInt(2, quantity);
            st.setDouble(3, price);
            st.setString(4, describe);
            st.setString(5, img);
            st.setInt(6, category);
            st.setInt(7, status);
            st.setString(8, id);
            st.executeUpdate();

        } catch (SQLException e) {

        }
    }

    public void deleteProduct(String pid) {
        String sql = "delete from Products where p_id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, pid);
            st.executeUpdate();
        } catch (SQLException e) {

        }
    }

    public static void main(String[] args) {
        DAO dao = new DAO();
//        List<Products> list = dao.filter("10", "900");
//        for (Products o : list) {
//            System.out.println(o.toString());
//        }
//        List<Account> list = dao.getAllAcc();
//        for (Account o : list) {
//            System.out.println(o.toString());
//        }
        int check = dao.updateNewPassword("admin@gmail.com", "1234");
        System.out.println(check);
//        List<Products> list = dao.getProductByManagerID(1);
//        for (Products o : list) {
//            System.out.println(o);
//        }
//        List<Category> list = dao.getAllCategory();
//        for (Category o : list) {
//            System.out.println(o);
//        }
    }
}
