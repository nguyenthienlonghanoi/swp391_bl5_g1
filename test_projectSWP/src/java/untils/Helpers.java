/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package untils;

import java.util.Random;

/**
 *
 * @author vanhv
 */
public class Helpers {

    public String randomOid() {
        String character = "ABCDEFGHIKLMNOPQRSTUVWXYZ123456789abcdefghiklmnopqrstuvwxyz";
        String random = "";
        int length = 10;
        Random rand = new Random();
        char[] text = new char[length];
        for (int i = 0; i < length; i++) {
            text[i] = character.charAt(rand.nextInt(character.length()));
        }
        for (int i = 0; i < text.length; i++) {
            random += text[i];
        }
        return random;
    }
}
