/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author tomlo
 */
public class Blog {

    public int id;
    public String image;
    public String title;
    public String context;
    public int author;
    public int status;
    public int numReact;
    public Timestamp createAt;

    public Blog() {
    }

    public Blog(int id, String image, String title, String context, int author, int status, int numReact, Timestamp createAt) {
        this.id = id;
        this.image = image;
        this.title = title;
        this.context = context;
        this.author = author;
        this.status = status;
        this.numReact = numReact;
        this.createAt = createAt;
    }

    public Blog(String image, String title, String context, int author, int status, int numReact, Timestamp createAt) {
        this.image = image;
        this.title = title;
        this.context = context;
        this.author = author;
        this.status = status;
        this.numReact = numReact;
        this.createAt = createAt;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public int getAuthor() {
        return author;
    }

    public void setAuthor(int author) {
        this.author = author;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getNumReact() {
        return numReact;
    }

    public void setNumReact(int numReact) {
        this.numReact = numReact;
    }

    public Timestamp getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Timestamp createAt) {
        this.createAt = createAt;
    }

    @Override
    public String toString() {
        return "Blog{" + "id=" + id + ", image=" + image + ", title=" + title + ", context=" + context + ", author=" + author + ", status=" + status + ", numReact=" + numReact + ", createAt=" + createAt + '}';
    }

    

}

