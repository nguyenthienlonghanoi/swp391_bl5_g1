/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author vanhv
 */
public class Cart {

    private String pId;
    private String pName;
    private String date;
    private String cQuantity, maxQuantity;
    private double price;
    private double total;

    public Cart() {
    }

    public Cart(String pId, String date, String pName, String cQuantity, double price, double total, String maxQuantity) {
        this.pId = pId;
        this.date = date;
        this.pName = pName;
        this.cQuantity = cQuantity;
        this.price = price;
        this.total = total;
        this.maxQuantity = maxQuantity;
    }

    public String getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(String maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getcQuantity() {
        return cQuantity;
    }

    public void setcQuantity(String cQuantity) {
        this.cQuantity = cQuantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return pId + ",date=" + date + ", pName=" + pName + ", cQuantity=" + cQuantity + ", price=" + price + ", total=" + total;
    }

}
