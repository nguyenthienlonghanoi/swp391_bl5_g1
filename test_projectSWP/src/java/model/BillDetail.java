/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author vanhv
 */
public class BillDetail {

    private String bdId, pid, bid;
    private int quantity;
    private double price, total;
    private String prodName,prodImg,prodDescribe;
    public BillDetail() {
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getProdImg() {
        return prodImg;
    }

    public void setProdImg(String prodImg) {
        this.prodImg = prodImg;
    }

    public String getProdDescribe() {
        return prodDescribe;
    }

    public void setProdDescribe(String prodDescribe) {
        this.prodDescribe = prodDescribe;
    }

    public BillDetail(int quantity, double price, double total, String prodName, String prodImg, String prodDescribe) {
        this.quantity = quantity;
        this.price = price;
        this.total = total;
        this.prodName = prodName;
        this.prodImg = prodImg;
        this.prodDescribe = prodDescribe;
    }

    public BillDetail(String bdId, String pid, String bid, int quantity, double price, double total) {
        this.bdId = bdId;
        this.pid = pid;
        this.bid = bid;
        this.quantity = quantity;
        this.price = price;
        this.total = total;
    }

    public BillDetail(String pid, String bid, int quantity, double price, double total) {
        this.pid = pid;
        this.bid = bid;
        this.quantity = quantity;
        this.price = price;
        this.total = total;
    }

    public String getBdId() {
        return bdId;
    }

    public void setBdId(String bdId) {
        this.bdId = bdId;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "BillDetail{" + "bdId=" + bdId + ", pid=" + pid + ", bid=" + bid + ", quantity=" + quantity + ", price=" + price + ", total=" + total + ", prodName=" + prodName + ", prodImg=" + prodImg + ", prodDescribe=" + prodDescribe + '}';
    }

   
    
}
