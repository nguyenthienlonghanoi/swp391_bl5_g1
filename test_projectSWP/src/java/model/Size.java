/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author vanhv
 */
public class Size {

    private int s_id;
    private String sizeName;

    public Size(int s_id, String sizeName) {
        this.s_id = s_id;
        this.sizeName = sizeName;
    }

    public Size() {
    }

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    @Override
    public String toString() {
        return "Size{" + "s_id=" + s_id + ", sizeName=" + sizeName + '}';
    }
    
}
