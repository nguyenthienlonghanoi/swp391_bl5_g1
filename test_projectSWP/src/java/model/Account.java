/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author dell
 */
public class Account {

    private int id;
    private String user, pass;
    private int seller, admin;
    private String email;
    private int status;

    public Account() {
    }

    public Account(int id, String user, String pass, int seller, int admin, String email, int status) {
        this.id = id;
        this.user = user;
        this.pass = pass;
        this.seller = seller;
        this.admin = admin;
        this.email = email;
        this.status = status;
    }

    public Account(int id, String user, String pass, int seller, int admin) {
        this.id = id;
        this.user = user;
        this.pass = pass;
        this.seller = seller;
        this.admin = admin;
    }

    public Account(int id, String user, String pass, int seller, int admin, String email) {
        this.id = id;
        this.user = user;
        this.pass = pass;
        this.seller = seller;
        this.admin = admin;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getSeller() {
        return seller;
    }

    public void setSeller(int seller) {
        this.seller = seller;
    }

    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }

    @Override
    public String toString() {
        return "Account{" + "id=" + id + ", user=" + user + ", pass=" + pass + ", seller=" + seller + ", admin=" + admin + ", email=" + email + ", status=" + status + '}';
    }


}
