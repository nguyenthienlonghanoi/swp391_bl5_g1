/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author dell
 */
public class Products {

    private String id, name;
    private int quantity;
    private double price;
    private String releaseDate, describe, image;
    private int cid, sid, coid, manager;
    private int status;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

   

    public Products() {
    }

    public Products(String id, int quantity) {
        this.id = id;
        this.quantity = quantity;
    }

    public Products(String id, String name, int quantity, double price, String releaseDate, String describe, String image, int cid, int sid, int coid, int manager, int status) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.releaseDate = releaseDate;
        this.describe = describe;
        this.image = image;
        this.cid = cid;
        this.sid = sid;
        this.coid = coid;
        this.manager = manager;
        this.status = status;
    }

    public Products(String id, String name, int quantity, double price, String releaseDate, String describe, String image, int cid, int sid, int coid, int manager) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.releaseDate = releaseDate;
        this.describe = describe;
        this.image = image;
        this.cid = cid;
        this.sid = sid;
        this.coid = coid;
        this.manager = manager;
    }

    public Products(String name, int quantity, double price, String describe, String image, int cid, int manager, int status) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.describe = describe;
        this.image = image;
        this.cid = cid;
        this.manager = manager;
        this.status = status;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getCoid() {
        return coid;
    }

    public void setCoid(int coid) {
        this.coid = coid;
    }

    public int getManager() {
        return manager;
    }

    public void setManager(int manager) {
        this.manager = manager;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Products{" + "id=" + id + ", name=" + name + ", quantity=" + quantity + ", price=" + price + ", releaseDate=" + releaseDate + ", cid=" + cid + ", sid=" + sid + ", coid=" + coid + ", manager=" + manager + '}';
    }

    public String addprod() {
        return "'" + name + "'," + quantity + "," + price + ",'" + describe + "','" + image + "'," + cid + "," + manager+","+status;
    }
}
