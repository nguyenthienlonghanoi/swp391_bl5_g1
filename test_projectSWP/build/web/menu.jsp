
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>

<!-- Offcanvas Menu Begin -->
<div class="offcanvas-menu-overlay"></div>
<div class="offcanvas-menu-wrapper">
    <div class="offcanvas__close">+</div>
    <ul class="offcanvas__widget">
        <li><span class="icon_search search-switch"></span></li>
        <li><a href="#"><span class="icon_heart_alt"></span>
                <div class="tip"></div>
            </a></li>
        <li><a href="#"><span class="icon_cart_alt"></span>
                <div class="tip"></div>
            </a></li>
    </ul>
    <div class="offcanvas__logo">
        <a href="./index.html"><img src="img/logo.png" alt=""></a>
    </div>
    <div id="mobile-menu-wrap"></div>
    <div class="offcanvas__auth">
        <a href="#">Login</a>
        <!--<a href="#">Register</a>-->
    </div>
</div>
<!-- Offcanvas Menu End -->

<!-- Header Section Begin -->
<header class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 col-lg-2">
                <div class="header__logo">
                    <a href="#"><img src="img/logo.png" alt=""></a>
                </div>
            </div>
            <div class="col-xl-6 col-lg-7">
                <nav class="header__menu">
                    <ul>
                        <li class="active"><a href="./shopp">Shop</a></li>
                            <c:if test="${sessionScope.acc.admin == 1||sessionScope.acc.seller == 1}">
                            <li>
                                <a href="#">Management</a>
                                <ul class="dropdown">
                                    <c:if test="${sessionScope.acc.admin == 1}">
                                        <li><a href="/managerAcc">Manage Account</a></li>
                                        </c:if>
                                        <c:if test="${sessionScope.acc.seller == 1}">
                                        <li><a href="/shop-management">Manage Product</a></li>
                                        <li><a href="/manage-bill">Statistic</a></li>
                                        </c:if>
                                </ul>
                            </li>
                        </c:if>
                        <li><a href="./blog">Blog</a>
                            <ul class="dropdown">
                                <li><a href="./addblog">Add Blog</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col-lg-3">
                <div class="header__right">
                    <c:if test="${sessionScope.acc == null}">
                        <div class="header__right__auth">
                            <a href="login.jsp">Login</a>
                        </div>
                    </c:if>
                    <c:if test="${sessionScope.acc != null}">
                        <div class="header__right__auth">
                            <a href="/shopp">Welcome ${acc.user}</a>
                        </div>
                        <div class="header__right__auth">
                            <a href="/changepass.jsp">Change password</a>
                            <a  href="/purchase">Purchase</a>
                            <a href="logout">Logout</a>
                        </div>
                    </c:if>
                    <ul class="header__right__widget">
                        <!--<li><span class="icon_search search-switch"></span></li>-->
                        <li><a href="/cartcontroller?service=showcart"><span class="icon_cart_alt"></span>
                                <c:if test="${sessionScope.listCart.size()>0}">
                                    <div class="tip">${sessionScope.listCart.size()}</div>
                                </c:if>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="canvas__open">
            <i class="fa fa-bars"></i>
        </div>
    </div>
</header>
<!-- Header Section End -->