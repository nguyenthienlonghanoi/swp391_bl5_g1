<%-- 
    Document   : purchase-detail
    Created on : Apr 25, 2023, 5:28:38 PM
    Author     : vanhv
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SPW391.G1 Purchase Detail</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <style>
        img{
            height: 120px;
            width: 100px
        }
    </style>
    <body class="container">
        <div class="table-wrapper">
            <div class="table-title">

                <div class="row">
                    <div class="col-sm-6">
                        <h2>Purchase Detail</h2>
                    </div>
                    <h3 class="text-danger" style="color: white"> ${mess}</h3>
                </div>
                <table class="table table-striped table-hover ">
                    <tr>
                        <th>Product</th>
                        <th>Image</th>
                        <th>Describe</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                    </tr>
                    <c:set var="grandTotal" value="${0}"/>
                    <c:forEach items="${listBillDetail}" var="list">
                        <tr>
                            <td>${list.prodName}</td>
                            <td>
                                <img src="${list.prodImg}"/>
                            </td>
                            <td>${list.prodDescribe}</td>
                            <td>${list.price}$</td>
                            <td>${list.quantity}</td>
                            <td>${list.total}$</td>
                        </tr>
                        <c:set var="grandTotal" value="${grandTotal+list.total}"/>
                    </c:forEach>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Grand total: ${grandTotal}$</th>
                    </tr>
                </table>
            </div>
            <a href="purchase"><button type="button" class="btn btn-primary">Go Back</button></a>
        </div>
    </body>
</html>
