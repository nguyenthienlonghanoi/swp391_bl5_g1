<%-- 
    Document   : showcart
    Created on : Apr 21, 2023, 12:28:47 AM
    Author     : vanhv
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SWP391.G1 - Giỏ hàng</title>
    </head>
    <style>
        body{
            margin:0px;
        }
        td,
        th {
            border: 2px solid white ;
            height: 70px;
            color:#fff;
            text-align: center;
        }
        th {
            font-size: 20px;
            font-weight: bolder;
            font-family: monospace;
        }
        td{
            font-family: monospace;
            font-size: 15px;
        }
        table{
            width: 98%;
            /*margin-left: 15px;*/
            background-color: cornflowerblue;
            border: 2px solid black;
            width: 100%;
            text-align: center;
            border-collapse: collapse;
        }
        a{
            text-decoration: none;
            color:#fff;
            font-weight: bold
        }
        .btnUpdate{
            font-size: 16px;
            margin-left: 20px;
            border: none;
            box-shadow:2px 2px 2px 1px #666666;
            padding: 15px 40px 15px 40px;
            color: white;
            background-color: deepskyblue;
        }
        .btnBack{
            margin-left: 50px;
            box-shadow:2px 2px 2px 1px #666666;
            background-color: black;
            padding: 16px 20px 15px 20px;
        }
        .btnCheckOut{
            box-shadow: 2px 2px 2px 1px #666666;
            float: right;
            margin-right: 22px;
            padding: 15px 40px 12px 40px;
            background-color: #2eaf55;
        }
        .btnRemoveAll{
            box-shadow: 2px 2px 2px 1px #666666;
            float: right;
            margin-right: 30px;
            padding: 15px 40px 12px 40px;
            background-color:red;
        }
        .btnBack,.btnCheckOut, .btnRemoveAll,.btnUpdate{
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
        }
        .allBtn{
            margin-top: 110px;
            display: block;
        }
        h4{
            color:#a6a6a6;
            text-align: center
        }
        .prodname{
            font-weight: bold
        }
        h2,h1,a{
            font-family: monospace
        }

    </style>
    <body>
        <h1>CART DETAIL</h1>
        <form method="POST" action="/cartcontroller">
            <table>
                <tr>
                    <th>Product Name</th>
                    <th>Order Date</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Total</th>
                    <th>Remove</th>
                </tr>
                <c:set var="grantTotal" value="${0}"/>
                <c:forEach items="${sessionScope.listCart}" var="carts">
                    <tr>
                        <td class="prodname">${carts.pName}</td>
                        <td>${carts.date}</td>
                        <td>
                            <input type="number" min="1" max="${carts.maxQuantity}" value="${carts.cQuantity}" name="p${carts.pId}">
                        </td>
                        <td>${carts.price}$</td>
                        <td>${carts.total}$</td>
                        <td><a href="/cartcontroller?service=remove&pid=${carts.pId}">REMOVE</a></td>
                    </tr>
                    <c:set var="grantTotal" value="${grantTotal+carts.total}"/>
                </c:forEach>
            </table>
            <c:if test="${sessionScope.listCart.size()==0}">
                <h4>Your cart is empty</h4>
            </c:if>
            <h2 style="margin-left:50px">Cart Total:  ${grantTotal}$</h2>
            <input type="hidden" name="service" value="update">
            <div class="allBtn">
                <a class="btnBack" href="/shopp">← Continue Shopping</a>
                <c:if test="${sessionScope.listCart.size()!=0}">
                    <button class="btnUpdate">Update</button>
                    <a class="btnCheckOut" href="/cartcontroller?service=checkout">Check Out →</a>
                    <a class="btnRemoveAll" href="/cartcontroller?service=removeAll">Remove All</a>
                </c:if>
            </div>
        </form>
    </body>
</html>
