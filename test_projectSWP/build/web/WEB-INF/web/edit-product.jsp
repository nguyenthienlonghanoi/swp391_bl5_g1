<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- Created By CodingLab - www.codinglabweb.com -->
<html lang="en" dir="ltr">
    <head>
        <meta charset="UTF-8">
        <!---<title> Responsive Registration Form | CodingLab </title>--->

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap');
            *{
                margin: 0;
                padding: 0;
                box-sizing: border-box;
                font-family: 'Poppins',sans-serif;
            }
            body{
                height: 100vh;
                display: flex;
                justify-content: center;
                align-items: center;
                padding: 10px;
                background: linear-gradient(135deg, #71b7e6, #9b59b6);
            }
            .container{
                max-width: 700px;
                width: 100%;
                background-color: #fff;
                padding: 25px 30px;
                border-radius: 5px;
                box-shadow: 0 5px 10px rgba(0,0,0,0.15);
            }
            .container .title{
                font-size: 25px;
                font-weight: 500;
                position: relative;
            }
            .container .title::before{
                content: "";
                position: absolute;
                left: 0;
                bottom: 0;
                height: 3px;
                width: 30px;
                border-radius: 5px;
                background: linear-gradient(135deg, #71b7e6, #9b59b6);
            }
            .content form .user-details{
                display: flex;
                flex-wrap: wrap;
                justify-content: space-between;
                margin: 20px 0 12px 0;
            }
            form .user-details .input-box{
                margin-bottom: 15px;
                width: calc(100% / 2 - 20px);
            }
            form .input-box span.details{
                display: block;
                font-weight: 500;
                margin-bottom: 5px;
            }
            .user-details .input-box input{
                height: 45px;
                width: 100%;
                outline: none;
                font-size: 16px;
                border-radius: 5px;
                padding-left: 15px;
                border: 1px solid #ccc;
                border-bottom-width: 2px;
                transition: all 0.3s ease;
            }
            .user-details .input-box input:focus,
            .user-details .input-box input:valid{
                border-color: #9b59b6;
            }

            form .category{
                display: flex;
                width: 80%;
                margin: 14px 0 ;
                justify-content: space-between;
            }
            form .category label{
                display: flex;
                align-items: center;
                cursor: pointer;
            }
            form .category label .dot{
                height: 18px;
                width: 18px;
                border-radius: 50%;
                margin-right: 10px;
                background: #d9d9d9;
                border: 5px solid transparent;
                transition: all 0.3s ease;
            }
            #dot-1:checked ~ .category label .one,
            #dot-2:checked ~ .category label .two,
            #dot-3:checked ~ .category label .three{
                background: #9b59b6;
                border-color: #d9d9d9;
            }
            form input[type="radio"]{
                display: none;
            }
            form .button{
                height: 45px;
                margin: 35px 0
            }
            form .button input{
                height: 100%;
                width: 100%;
                border-radius: 5px;
                border: none;
                color: #fff;
                font-size: 18px;
                font-weight: 500;
                letter-spacing: 1px;
                cursor: pointer;
                transition: all 0.3s ease;
                background: linear-gradient(135deg, #71b7e6, #9b59b6);
            }
            form .button input:hover{
                /* transform: scale(0.99); */
                background: linear-gradient(-135deg, #71b7e6, #9b59b6);
            }
            @media(max-width: 584px){
                .container{
                    max-width: 100%;
                }
                form .user-details .input-box{
                    margin-bottom: 15px;
                    width: 100%;
                }
                form .category{
                    width: 100%;
                }
                .content form .user-details{
                    max-height: 300px;
                    overflow-y: scroll;
                }
                .user-details::-webkit-scrollbar{
                    width: 5px;
                }
            }
            @media(max-width: 459px){
                .container .content .category{
                    flex-direction: column;
                }
            }
            .switch {
                position: relative;
                display: inline-block;
                width: 50px;
                height: 24px;
            }

            .switch input {
                opacity: 0;
                width: 0;
                height: 0;
            }

            .slider {
                position: absolute;
                cursor: pointer;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: #ccc;
                -webkit-transition: .4s;
                transition: .4s;
            }

            .slider:before {
                position: absolute;
                content: "";
                height: 16px;
                width: 16px;
                left: 4px;
                bottom: 4px;
                background-color: white;
                -webkit-transition: .4s;
                transition: .4s;
            }

            input:checked + .slider {
                background-color: #2196F3;
            }

            input:focus + .slider {
                box-shadow: 0 0 1px #2196F3;
            }

            input:checked + .slider:before {
                -webkit-transform: translateX(26px);
                -ms-transform: translateX(26px);
                transform: translateX(26px);
            }

            /* Rounded sliders */
            .slider.round {
                border-radius: 34px;
            }

            .slider.round:before {
                border-radius: 50%;
            }

        </style>
    </head>

    <body>
        <div class="container">
            <div class="title">Contract</div>
            <div class="content">
                <form action="/edit" method="post">
                    <div class="user-details">
                        <div class="input-box">
                            <span class="details">ID</span>
                            <input name="id" value="${detail.id}" type="text" readonly placeholder="Enter your name" required>
                        </div>
                        <div class="input-box">
                            <span class="details">Name</span>
                            <input name="name" value="${detail.name}" type="text" placeholder="Enter mobile number" required>
                        </div>
                        <div class="input-box">
                            <span class="details">Image</span>
                            <input name="image" value="${detail.image}" type="text" placeholder="Enter start date" required>
                        </div>
                        <div class="input-box">
                            <span class="details">Quantity</span>
                            <input name="quantity" value="${detail.quantity}" type="text" placeholder="Enter end date" required>
                        </div>
                        <div class="input-box">
                            <span class="details">Price</span>
                            <input name="price" value="${detail.price}" type="text" placeholder="Enter reason" required>
                        </div>
                        <div class="input-box">
                            <span class="details">Describe</span>
                            <input name="describe" value="${detail.describe}" type="text" placeholder="Enter reason" required>
                        </div>
                        <div class="input-box">
                            <span class="details">Status</span>
                            <label class="switch">
                                <input type="checkbox" ${detail.status==0?"":"checked"} name="status">
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="input-box">
                            <label>Category</label>
                            <select name="category">
                                <c:forEach items="${listC}" var="o">
                                    <option value="${o.id}">${o.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="button">
                        <input type="submit" value="Next">
                    </div>
                </form>
            </div>
        </div>

    </body>
</html>
