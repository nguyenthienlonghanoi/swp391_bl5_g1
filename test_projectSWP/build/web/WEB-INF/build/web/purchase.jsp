<%-- 
    Document   : purchase
    Created on : Apr 25, 2023, 2:30:05 PM
    Author     : vanhv
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SWP391.G1 Purchase</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <style>
        td,
        th {
            color:black;
            text-align: center;
        }
        th {
            font-size: 18px;
            font-weight: bolder;
            font-family: monospace;
        }
        td{
            font-family: monospace;
            font-size: 14px;
        }
    </style>
    <body class="container">
        <div class="table-wrapper">
            <div class="table-title">

                <div class="row">
                    <div class="col-sm-6">
                        <h2>My Purchase</h2>
                    </div>
                    <h3 class="text-danger" style="color: white"> ${mess}</h3>
                </div>
            </div>
            <table class="table table-striped table-hover ">
                <tr>
                    <th>Code</th>
                    <th>Order Date</th>
                    <th>Receiver</th>
                    <th>Phone</th>
                    <th>Delivery Address</th>
                    <th>Total</th>
                    <th>Status</th>
                    <th></th>
                    <th></th>
                </tr>
                <c:forEach items="${listBill}" var="list">
                    <tr>
                        <td>${list.oID}</td>
                        <td>${list.dateCreate}</td>
                        <td>${list.cname}</td>
                        <td>${list.cphone}</td>
                        <td>${list.cAddress}</td>
                        <td>${list.total}$</td>
                        <td style="color:${list.status==0?"#ff9900"
                                           :(list.status==1?"#0099ff"
                                           :(list.status==-1?"#ff0000"
                                           :"#00cc00"))};
                            font-weight:600 ">
                            ${list.status==0?"WAITING"
                              :(list.status==1?"DELIVERING"
                              :(list.status==-1?"CANCELED"
                              :"RECEIVED"))}
                        </td>
                        <td>
                            <a href="purchase?service=detail&b_id=${list.oID}">
                                <button type="button" class="btn-secondary">Detail</button>
                            </a>
                        </td>
                        <c:if test="${list.status==0}">
                            <td>
                                <a href="purchase?service=cancel&b_id=${list.oID}">
                                    <button type="button" class="btn-danger">Cancel</button>
                                </a>
                            </td>
                        </c:if>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <a href="shopp"><button type="button" class="btn btn-primary">Back to Shop</button></a>
    </body>
</html>
