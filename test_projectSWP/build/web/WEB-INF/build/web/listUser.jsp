<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/managerdoctor.css"/>
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="adminView.jsp">User manager</a>
            <!-- Sidebar Toggle-->

            <!-- Navbar Search-->
            <form  action="searchbook" method="post"  class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input name="txt" class="form-control" type="text" placeholder="Search for..." aria-label="Search for..." aria-describedby="btnNavbarSearch" />
                    <button class="btn btn-primary" id="btnNavbarSearch" type="submit" ><i class="fas fa-search"></i></button>
                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="/profile">Profile</a></li>
                        <li><a class="dropdown-item" href="/change">ChangePass</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="/logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>

        <div id="table-head">
            <main>
                <div class="container-fluid px-4">
                    <div class="card-header text-xl-left">
                        <i class="fas fa-table me-1 " ></i> 
                        User
                    </div>
                    <table class="table table-bordered">
                        <thead id="table-head">
                            <tr>
                                <th class="text-center">ID</th>                                        
                                <th class="text-center">Name Of user</th>
                                <th class="text-center">Date of birth</th>   
                                <th class="text-center">Gender</th>
                                <th class="text-center">Address</th>
                                <th class="text-center">Mail</th>
                                <th class="text-center">Phone</th>
                                <th class="text-center">Account</th>
                                <th class="text-center">Password</th>
                                <th class="text-center">Role</th>
                            </tr>
                        </thead>
                        <c:forEach items="${listP}" var="p">                               
                            <tbody>
                                <tr class="align-items-center">
                                    <td class="text-center">${p.profileID}</td>                                        
                                    <td class="text-center">${p.firstName} ${p.middleName} ${p.lastName}</td>
                                    <td class="text-center">${p.doB}</td>    
                                    <td class="text-center">${p.gender?"Male":"Famale"}</td>
                                    <td class="text-center">${p.address}</td>
                                    <td class="text-center">${p.mail}</td>
                                    <td class="text-center">${p.phone}</td>
                                    <td class="text-center">${p.account}</td>
                                    <td class="text-center">${p.password}</td>
                                    <td class="text-center">${p.role}</td>
                                    <td class="text-center align-middle">
                                        <div class="btn-group align-top">
                                            <button class="btn btn-sm btn-outline-secondary "  type="button" data-toggle="modal" data-target="#user-form-modal">Edit</button>
                                            <a class="btn btn-sm btn-outline-secondary" onclick="show(${p.profileID})" target="__blank" href="#">Delete</a> 
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </c:forEach> 

                    </table>
                    <a href="addUser" class="btn btn-sm btn-outline-secondary "  type="button" data-toggle="modal" data-target="#user-form-modal">Add new User</a>
                    <ul class="pagination ">
                        <li class="page-item"><a class="page-link" href="listUser?index=${tag - 1}">Previous</a></li>
                            <c:forEach  begin="1" end="${endP}" var="i">        
                            <li class="page-item ${tag == i? "active":""}"><a class="page-link" href="listUser?index=${i}">${i}</a></li>
                            </c:forEach>
                        <li class="page-item"><a class="page-link" href="listUser?index=${tag + 1}">Next</a></li>
                    </ul>               
            </main>

        </div>


        <!-- ================ -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts1.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="js/scripts2.js"></script>
    </body>
    <script>

                                            function show(id) {
                                                var option = confirm('Are you sure to delete');
                                                if (option === true) {
                                                    window.location.href = 'deleteUser?id=' + id;
                                                }

                                            }
    </script>
</html>
