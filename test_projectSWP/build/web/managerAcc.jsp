<%-- 
    Document   : managerACc
    Created on : Mar 15, 2022, 8:31:40 AM
    Author     : Thanh Thao
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Manager Account</title>
        
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="assets/css/manager.css" rel="stylesheet" type="text/css"/>

        <!-- Jquery -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <!-- Owl caroucel Js-->
        <script src="assets/owlCarousel/owl.carousel.min.js"></script>
        <style>
            img{
                width: 200px;
                height: 120px;
            }

        </style>
        <!--        <script>
                    $(document).ready(function () {
                        $('#dtTableProduct').DataTable();
                        $('.dataTables_length').addClass('bs-select');
                    });
                </script>-->
    </head>
    <body>

        <div class="container">
            <div class="table-wrapper">

                <div class="table-title">

                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Manage <b>Account</b></h2>
                        </div>
                        <h3 class="text-danger" style="color: white"> ${mess}</h3>
                    </div>
                </div>
                <table  class="table table-striped table-hover ">
                    <thead>
                        <tr>
                            <th>AccountName</th>
                            <th>Username</th>
                            <th>Status</th>
                            <th>Block/UnBlock</th>
                            <!--<th>Block</th>-->
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${requestScope.account}" var="o">
                            <tr>
                                <td>${o.user}</td>
                                <td>${o.user}</td>
                                <c:if test="${o.status==-1}">
                                    <td style="">
                                        <p style="color:red;font-weight:600">BLOCKED</p
                                        ></td>
                                    </c:if>
                                    <c:if test="${o.status!=-1}">
                                    <td style="">
                                        <p style="color:#33cc33;font-weight:600">ACTIVE</p>
                                    </td>
                                </c:if>
                                <c:if test="${o.status == -1}">
                                    <td>
                                        <a style="width: 100%;" href="unblock?aid=${o.id}" onclick="return confirm('You want to UnBlock Account?');"  class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="UnBlock">lock_open</i></a>
                                    </td>
                                </c:if>
                                <c:if test="${o.status != -1}">
                                    <td>
                                        <a style="width: 100%;color:red;" href="block?aid=${o.id}" onclick="return confirm('You want to Block Account?');" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Block">lock_outline</i></a>
                                    </td>
                                </c:if>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>

            </div>
            <a href="shopp"><button type="button" class="btn btn-primary">Back to home</button></a>

        </div>



        <script src="assets/js/manager.js" type="text/javascript"></script>
    </body>
</html>