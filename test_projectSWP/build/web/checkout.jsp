<%-- 
    Document   : checkout
    Created on : Apr 22, 2023, 12:06:35 PM
    Author     : vanhv
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SWP391.G1 - Thanh toán đơn hàng</title>
    </head>
    <body>
        <form method="POST" action="/cartcontroller" >
            <div class="wrap">
                <div class="wrapLeft">
                    <h3 class="titleWrap">Thông tin giao hàng</h3>
                    <div class="inputwrap">
                        <input placeholder="Họ và Tên" name="name" type="text" required/>
                        <input placeholder="Số điện thoại" name="phone" type="text" required/>
                        <input placeholder="Địa chỉ giao hàng" maxlength="99" name="address" type="text" required/>
                        <a href="/cartcontroller?service=showcart">Giỏ hàng</a>
                        <input type="submit" value="Hoàn tất đơn hàng"  class="done"/>
                        <input hidden name="service" value="payment"/>
                    </div>
                </div>
                <div class="wrapRight">
                    <h3 class="titleWrap">Giỏ hàng</h3>
                    <c:set var="grantTotal" value="${0}"/>
                    <c:forEach items="${sessionScope.listCart}" var="list">
                        <div style="display: flex;justify-content: space-between">
                            <p style="padding-left: 100px">${list.pName}</p>
                            <p style="padding-left: 50px">${list.cQuantity}</p>
                            <p style="padding-right: 50px">${list.price}$</p>
                        </div>
                        <div style="border-bottom: 1px solid black"></div>
                        <c:set var="grantTotal" value="${grantTotal+list.total}"/>
                    </c:forEach>
                    <h2 style="padding-left: 100px">Tổng cộng:  ${grantTotal}$</h2>
                </div>
            </div>
        </form>
    </body>
</html>
<style>
    input{
        padding: 10px;
        width: 100%;
        margin-bottom: 10px;
    }
    .inputwrap{
        margin-left: 150px;
        margin-right:150px;
    }
    a,
    a:active,
    a:visited {
        color: blue;
        font-family: monospace;
        text-decoration: none;
    }
    .done{
        text-align: center;
        background-color: #80aaff;
        padding:10px;
        margin-top: 20px;
        margin-left: 30px;
        width: 40%;
        border: 0;
        font-family: monospace;
        color:white;
    }
    h3,p,h2{
        font-family: monospace;
    }
    p{
        font-size: 17px;
    }
    .wrap{
        display: flex;
        justify-content: space-between;
    }
    .wrapLeft{
        flex:1;
        padding-left: 40px;
    }
    .wrapRight{
        /*background-color: #80aaff;*/
        flex:1;
    }
    .titleWrap{
        text-align: center
    }
</style>